package org.crypthing.things.appservice.management;

import javax.naming.InitialContext;

/**
 * Monitored applications JSON file.
 */
public class Storage
{
	public static final String RESOURCE ="java:global/things/storage";
	public static final String storage;
	static
	{
		try { storage = (String) new InitialContext().lookup(RESOURCE); }
		catch (final Exception e) { throw new RuntimeException("Could not locate the resource:" + RESOURCE, e); }
	}
}
