package org.crypthing.things.appservice.management.data;

import java.util.HashMap;
import java.util.Map;

/**
 * Applications under monitoring
 */
public class Configuration
{
	private Map<String, Server> servers = new HashMap<String,Server>();

	/**
	 * Gets detected applications.
	 * @return a map of applications.
	 */
	public Map<String, Server> getServers() { return servers; }

	/**
	 * Sets applications.
	 * @param servers: map of detected applications
	 */
	public void setServers(Map<String, Server> servers) { this.servers = servers; }
}
