package org.crypthing.things.appservice.management.data;

/**
 * Application heartbeat signal beans
 */
public class HeartBeat
{
	/**
	 * Applications types
	 */
	public enum Type { AGENT, SERVER };

	private JMX jmx;
	private Type type = Type.SERVER;
	private long success;
	private long failures;
	private long workers;

	/**
	 * Gets application JMX address
	 */
	public JMX  getJmx() { return jmx; }
	/**
	 * Sets application JMX address
	 * @param jmx: JMX address.
	 */
	public void setJmx(final JMX jmx) { this.jmx = jmx;} 
	/**
	 * Gets application type
	 * @return JMX address
	 */
	public Type getType() { return type; }
	/**
	 * Sets application type
	 * @param type: application type.
	 */
	public void setType(final Type type) { this.type = type; }
	/**
	 * Gets application success counter.
	 * @return successes
	 */
	public long getSuccess() { return success; }
	/**
	 * Sets application success counter
	 * @param success: successes
	 */
	public void setSuccess(final long success) { this.success = success; }
	/**
	 * Gets application failures counter
	 * @return failures
	 */
	public long getFailures() { return failures; }
	/**
	 * Sets application failure counter
	 * @param failures: failures
	 */
	public void setFailures(final long failures) { this.failures = failures; }
	/**
	 * Gets application workers counter
	 * @return workers
	 */
	public long getWorkers() { return workers; }
	/**
	 * Sets application workers counter
	 * @param workers: workers
	 */
	public void setWorkers(final long workers) { this.workers = workers; }
}
