package org.crypthing.things.appservice.management.data;

/**
 * Application JMX address bean
 */
public class JMX
{
	private String address;
	private int port;

	/**
	 * Gets application JMX IP address
	 * @return IP number
	 */
	public String getAddress() { return address; }
	/**
	 * Sets application JMX IP address
	 * @param address: IP address
	 */
	public void setAddress(final String address) { this.address = address; }
	/**
	 * Gets application JMX port
	 * @return port number
	 */
	public int getPort() { return port; }
	/**
	 * Sets application JMX port
	 * @param port: port number
	 */
	public void setPort(final int port) { this.port = port; }
}
