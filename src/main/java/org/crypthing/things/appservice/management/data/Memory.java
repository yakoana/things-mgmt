package org.crypthing.things.appservice.management.data;

/**
 * JVM memory usage bean
 */
public class Memory
{
	private long init;
	private long used;
	private long committed;
	private long max;

	/**
	 * Creates a new instance of memory usage
	 */
	public Memory() {}
	/**
	 * Creates a new instance of memory usage
	 * @param init: initial memory
	 * @param used: used memory
	 * @param committed: committed memory
	 * @param max: maximum memory
	 */
	public Memory(final long init, final long used, final long committed, final long max)
	{
		this.init = init;
		this.used = used;
		this.committed = committed;
		this.max = max;
	}
	/**
	 * Gets JVM maximum memory
	 * @return max memory
	 */
	public long getMax() { return max; }
	/**
	 * Sets JVM maximum memory
	 * @param max: max memory
	 */
	public void setMax(final long max) { this.max = max; }
	/**
	 * Gets JVM committed memory
	 * @return commited memory
	 */
	public long getCommitted() { return committed; }
	/**
	 * Sets JVM commited memory
	 * @param committed: committed memory
	 */
	public void setCommitted(final long committed) { this.committed = committed; }
	/**
	 * Gets JVM used memory
	 * @return used memory
	 */
	public long getUsed() { return used; }
	/**
	 * Sets JVM used memory
	 * @param used: used memory
	 */
	public void setUsed(final long used) { this.used = used; }
	/**
	 * Gets JVM initial memory
	 * @return initial memory
	 */
	public long getInit() { return init; }
	/**
	 * Sets JVM initial memory
	 * @param init: initial memory
	 */
	public void setInit(final long init) { this.init = init; }
  }