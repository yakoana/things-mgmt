package org.crypthing.things.appservice.management.data;

import java.lang.management.MemoryUsage;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Application bean
 */
public class Server
{
	private String name;
	private String config;
	@JsonProperty("env")
	private String[] env;
	private JMX jmx;
	private long lastknowof;
	private long startTime;
	private long success;
	private long failures;
	private long workers;
	private Memory heap;
	private Memory nonheap;
	private long heartbeats;
	@JsonIgnore
	private boolean _shutdown = false;

	/**
	 * Application has received a shutdown signal
	 */
	@JsonIgnore
	public void shutdownSignal() { _shutdown = true; }
	/**
	 * Asks if application has received a shutdown signal
	 * @return true if application is shuting down
	 */
	@JsonIgnore
	public boolean isShutingdown() { return _shutdown; }

	/**
	 * Increments application heartbeats
	 */
	@JsonIgnore
	public long beat() { return heartbeats++; }
	/**
	 * Gets application heartbeats
	 * @return hearbeats counter
	 */
	public long getHeartbeats() { return heartbeats; }
	/**
	 * Sets application heartbeats
	 * @param heartbeats: heartbeats
	 */
	public void setHeartbeats(final long heartbeats) { this.heartbeats = heartbeats; }
	/**
	 * Gets application name
	 * @return application name
	 */
	public String getName() { return name; }
	/**
	 * Sets application name
	 * @param name: application name
	 */
	public void setName(final String name) { this.name = name; }
	/**
	 * Gets application configuration file
	 * @return the configuration file path
	 */
	public String getConfig() { return config; }
	/**
	 * Sets application configuration file path
	 * @param config: file path
	 */
	public void setConfig(final String config) { this.config = config; }
	/**
	 * Gets application environment
	 * @return application environment
	 */
	public String[] getEnv() { return env; }
	/**
	 * Sets application environment
	 * @param env: application environment
	 */
	public void setEnv(final String[] env) { this.env = env; }
	/**
	 * Sets application JMX address
	 * @return JMX address
	 */
	public JMX getJmx() { return jmx; }
	/**
	 * Sets application JMX address
	 * @param jmx: JMX address
	 */
	public void setJmx(final JMX jmx) { this.jmx = jmx; }
	/**
	 * Gets application last signal timestamp
	 * @return last signal timestamp
	 */
	public long getLastknowof() { return lastknowof; }
	/**
	 * Sets application last signal timestamp
	 * @param lastknowof: last signal timestamp
	 */
	public void setLastknowof(final long lastknowof) { this.lastknowof = lastknowof; }
	/**
	 * Gets application start time
	 * @return start time
	 */
	public long getStartTime() { return startTime; }
	/**
	 * Sets application start time
	 * @param startTime: start time
	 */
	public void setStartTime(final long startTime) { this.startTime = startTime; }
	/**
	 * Gets application failures counter
	 * @return failures counter
	 */
	public long getFailures() { return failures; }
	/**
	 * Sets application failures counter
	 * @param failures: failures counter
	 */
	public void setFailures(final long failures) { this.failures = failures; }
	/**
	 * Gets application successes counter
	 * @return successes counter
	 */
	public long getSuccess() {return success;}
	/**
	 * Sets application successes counter
	 * @param success: counter
	 */
	public void setSuccess(final long success) { this.success = success; }
	/**
	 * Gets application workers
	 * @return workers
	 */
	public long getWorkers() { return workers; }
	/**
	 * Sets application workers
	 * @param workers: counter
	 */
	public void setWorkers(final long workers) { this.workers = workers; }
	/**
	 * Gets application heap memory bean
	 * @return heap memory
	 */
	public Memory getHeap() { return heap; }
	/**
	 * Sets application heap memory
	 * @param heap: heap memory
	 */
	public void setHeap(final Memory heap) { this.heap = heap; }
	/**
	 * Gets non-heap application memory bean
	 * @return non-heap memory
	 */
	public Memory getNonheap() { return nonheap; }
	/**
	 * Sets application non-heap memory bean
	 * @param nonheap: non-heap memory bean
	 */
	public void setNonheap(Memory nonheap) { this.nonheap = nonheap; }
	/**
	 * Converts a JVM heap memory usage report in a memory bean
	 * @param heap: heap memory usage report
	 */
	@JsonIgnore
	public void setHeap(final MemoryUsage heap)
	{
		this.heap = new Memory
		(
			heap.getInit(),
			heap.getUsed(),
			heap.getCommitted(),
			heap.getMax()
		);
	}
	/**
	 * Converts a JVM non-heap memory usage report in a memory bean
	 * @param nonheap: non-heap memory usage report
	 */
	@JsonIgnore
	public void setNonheap(final MemoryUsage nonheap)
	{
		this.nonheap = new Memory
		(
			nonheap.getInit(),
			nonheap.getUsed(),
			nonheap.getCommitted(),
			nonheap.getMax()
		);
	}
}
