package org.crypthing.things.appservice.management.data;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.crypthing.things.appservice.management.Storage;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Stores Things application under monitoring
 */
public class ThingsServers
{
	private static final Logger log = Logger.getLogger(ThingsServers.class.getName());
	private static final Map<String, ThingsServers> map = new HashMap<String, ThingsServers>(); 

	/**
	 * Creates a new Things applications storage
	 * @return a new instance
	 * @throws IOException if persistent JSON file should be loaded and it fails
	 */
	public static ThingsServers getInstance() throws IOException
	{
		return getInstance(Storage.storage);
	}
	/**
	 * Greates a new Things application storage
	 * @param base: storage type name, e.g., rogue servers, servers, agent
	 * @return a new instance
	 * @throws IOException if persistent JSON file should be loaded and it fails
	 */
	public static ThingsServers getInstance(final String base) throws IOException
	{
		ThingsServers servers = map.get(base);
		if (servers == null) 
		{ 
			servers = new ThingsServers(base);
			servers.load();
			map.put(base,  servers);
		}
		return servers;
	}
	
	private ThingsServers(String base) { storage = base; }
	private String storage;
	private Map<String, Server> servermap = new HashMap<String, Server>();
	private final Map<String, Server> rogueservermap = new HashMap<String, Server>();
	private final Map<String, Server> agentmap = new HashMap<String, Server>();
	private Configuration conf;
	private final ObjectMapper mapper = new ObjectMapper();

	/**
	 * Loads monitoring applications JSON file from persistence
	 * @throws IOException
	 */
	public void load() throws IOException
	{
		final File f = new File(storage);
		if (f.exists())
		{
			try { conf = mapper.readValue(f, Configuration.class); }
			catch (final Exception e) {  log.log(Level.SEVERE, "An error occurred while load monitoring applications JSON file", e); }
		}
		if (conf == null)
		{
			conf = new Configuration();
			store();
		}
		servermap = conf.getServers();
	}
	/**
	 * Stores monitoring applications JSON file
	 * @throws IOException if something goes wrong
	 */
	public void store() throws IOException
	{
		final File f = new File(storage);
		mapper.writeValue(f, conf);
	}

	/**
	 * Gets monitoring applications
	 * @return map of monitoring applications
	 */
	public Map<String, Server> getServers() { return servermap; }
	/**
	 * Gets specified monitoring application bean
	 * @param name: monitoring application name
	 * @return application bean
	 */
	public Server getServer(String name) { return servermap.get(name); 	}
	/**
	 * Adds a new monitoring application
	 * @param server: application bean
	 */
	public void addServer(final Server server) { /** Nullpointer expected if you are not careful and send a null server. */ servermap.put(server.getName(), server); }
	/**
	 * Removes application from monitoring map
	 * @param server: application bean
	 */
	public void removeServer(final Server server) { /** Nullpointer expected if you are not careful and send a null server. */ servermap.remove(server.getName()); }
	/**
	 * Removes application from monitoring map
	 * @param name: application name
	 */
	public void removeServer(final String name) { servermap.remove(name); }

	/**
	 * Gets non-monitoring applications map
	 * @return non-monitoring applications
	 */
	public Map<String, Server> getRogueServers() { return rogueservermap; }
	/**
	 * Gets specified non-monitoring application bean
	 * @param rogueserver: application name
	 * @return application bean
	 */
	public Server getRogueServer(final String rogueserver) { return this.rogueservermap.get(rogueserver); }
	/**
	 * Adds a new non-monitoring application
	 * @param server: application bean
	 */
	public void addRogueServer(final Server server) { /** Nullpointer expected if you are not careful and send a null server. */ rogueservermap.put(server.getName(), server); }
	/**
	 * Removes application from non-monitoring map
	 * @param server: application bean
	 */
	public void removeRogueServer(final Server server) { /** Nullpointer expected if you are not careful and send a null server. */ rogueservermap.remove(server.getName()); }
	/**
	 * Removes application from non-monitoring map
	 * @param name: application name
	 */
	public void removeRogueServer(final String name) { rogueservermap.remove(name); }

	/**
	 * Gets Things agents map
	 * @return agents map
	 */
	public Map<String, Server> getAgents(){ return agentmap; }
	/**
	 * Gets specified Things agent bean
	 * @param name: agent name
	 * @return agent bean
	 */
	public Server getAgent(final String name) { return agentmap.get(name); }
	/**
	 * Adds a new agent to Things agents map
	 * @param agent: agent bean
	 */
	public void addAgent(final Server agent) { /** Nullpointer expected if you are not careful and send a null server. */ agentmap.put(agent.getName(), agent); }
	/**
	 * Removes an agent from Things agents map
	 * @param agent: agent bean
	 */
	public void removeAgent(final Server agent) { /** Nullpointer expected if you are not careful and send a null server. */ agentmap.remove(agent.getName()); }
	/**
	 * Removes an agent from Things agents map
	 * @param name: agent name
	 */
	public void removeAgent(final String name) { agentmap.remove(name); }
}
