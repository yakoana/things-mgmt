package org.crypthing.things.appservice.management.mib;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import javax.naming.spi.NamingManager;

/**
 * Workoaround to JBoss EAP 7.1
 */
public class JJRMI implements InitialContextFactory
{
	@Override
	public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException
	{
		return NamingManager.getURLContext("rmi", environment);
	}
}