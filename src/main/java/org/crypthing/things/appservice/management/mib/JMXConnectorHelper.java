package org.crypthing.things.appservice.management.mib;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.management.MemoryUsage;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;

import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeDataSupport;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.crypthing.things.appservice.Bootstrap;
import org.crypthing.things.appservice.config.JVMConfig;
import org.crypthing.things.config.ConfigException;
import org.crypthing.things.appservice.management.data.Server;
import org.crypthing.things.appservice.Runner;

/**
 * JMX connection facility
 */
public class JMXConnectorHelper
{
    private static final Logger log = Logger.getLogger(JMXConnectorHelper.class.getName());

	/**
	 * Gets connector to specified application
	 * @param server: application bean
	 * @return a JMX connector
	 * @throws IOException if JMX connection fails
	 * @throws ConfigException if cannot get application JMX address
	 */
	public static JMXConnector getConnector(final Server server) throws IOException, ConfigException
	{
		{
			String host;
			int port;
			File f;
			if (server.getJmx() != null)
			{
				host = server.getJmx().getAddress();
				port = server.getJmx().getPort();
			}
			else if (server.getConfig() != null && (f = new File(server.getConfig())).exists() && f.canRead())
			{
				try
				{
					final JVMConfig cfg = Bootstrap.getJVMConfig(new FileInputStream(f), Bootstrap.getSchema());
					if (cfg.getJmx() == null) throw new ConfigException("Configuration must have a JMX entry");
					host = cfg.getJmx().getHost();
					port = Integer.parseInt(cfg.getJmx().getPort());
				}
				catch (final Exception e)
				{
					log.log(Level.WARNING, "Unable to recognize JMX for Server:" + server.getName(), e);
					return null;
				}
			}
			else
			{
				log.log(Level.WARNING, "Unable to find JMX informations for Server:" + server.getName());
				return null;
			}
			final Map<String, Object> env = new HashMap<String, Object>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, JJRMI.class.getName());
			env.put(Context.PROVIDER_URL, "rmi://" + host + ":" + port);
			final JMXServiceURL address = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/jmxrmi");
			return JMXConnectorFactory.connect(address, env);
		}
    }

	/**
	 * Updates application status
	 * @param server: application bean
	 * @return true if status could be updated; otherwise, false.
	 */
	public static boolean updateServerStatus(Server server)
	{
		try
		{
			log.info("Updating server " + server.getName());
			final JMXConnector jmxc = getConnector(server);
			final MBeanServerConnection mbean = jmxc.getMBeanServerConnection();
			final MBeanGetter get = new MBeanGetter(mbean);
			final Iterator<ObjectName> it = mbean.queryNames(new ObjectName(Runner.MBEAN_PATTERN + "*"), null).iterator();
			final ObjectName obName = it.hasNext() ? it.next() : null;
			if (obName == null)
			{
				log.info("Given URL has no Runner " + Runner.MBEAN_PATTERN + " or it was not started yet");
				return false;
			}
			log.fine("MBean found for server " + server.getName());

			final long lastMeasure = System.currentTimeMillis();
			final long success = get.getValue(obName, "SuccessCount");
			final long failures = get.getValue(obName, "ErrorCount");
			final int workers = get.getValue(obName, "WorkerCount");
			final String config = get.getValue(obName, "ConfigFile");
			final String[] environment = get.getValue(obName, "Environment");
			final ObjectName runtime = new ObjectName("java.lang:type=Runtime");
			final ObjectName memory = new ObjectName("java.lang:type=Memory");

			CompositeDataSupport cp = (CompositeDataSupport) get.getValue(memory, "HeapMemoryUsage");
			final MemoryUsage heap = MemoryUsage.from(cp);
			cp = (CompositeDataSupport) get.getValue(memory, "NonHeapMemoryUsage");
			final MemoryUsage nonheap = MemoryUsage.from(cp);

			final long startTime = get.getValue(runtime, "StartTime");
			if (server.getStartTime() != startTime) server.setStartTime(startTime);
			server.setEnv(environment);
			server.setLastknowof(lastMeasure);
			server.setSuccess(success);
			server.setFailures(failures);
			server.setWorkers(workers);
			server.setConfig(config);
			server.setHeap(heap);
			server.setNonheap(nonheap);
			return true;
		}
		catch (final Exception e) { log.log(Level.WARNING, "Could not update server:" + server.getName(), e); }
		return false;
	}

	private static class MBeanGetter
	{
		final MBeanServerConnection mbean;
		public MBeanGetter(final MBeanServerConnection mbean) { this.mbean = mbean; }
		@SuppressWarnings("unchecked")
		private <T> T getValue
		(
			final ObjectName obName,
			final String attr
		)	throws	AttributeNotFoundException,
					InstanceNotFoundException,
					MBeanException,
					ReflectionException,
					IOException { return (T)mbean.getAttribute(obName, attr); }
	}
}