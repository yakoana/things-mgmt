package org.crypthing.things.appservice.management.mib;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;

import org.crypthing.things.appservice.management.data.HeartBeat;
import org.crypthing.things.appservice.management.data.Server;
import org.crypthing.things.appservice.management.data.ThingsServers;
import org.json.JSONObject;
import org.snmp4j.CommandResponder;
import org.snmp4j.CommandResponderEvent;
import org.snmp4j.PDU;

/**
 * Implements the SNMP listener
 */
public class MIB implements CommandResponder
{
	private static final String AGENT = "org.crypthing.things.appservice.Bootstrap";
	private static final String AGENT_SHORTCUT = "Bootstrap";
	private static final String SERVICE = "org.crypthing.things.appservice.Runner";
	private static final String SERVICE_SHORTCUT = "Runner";
	private static final long DELAY = 5000;
	private static final Logger LOG = Logger.getLogger(MIB.class.getName());

	private String mibAddress;
	private ThingsServers servers;
	private TrapReceiver trap;
	private boolean running;
	private boolean mustLog;

	/**
	 * Creates a new instance of Management Information Base
	 */
	public MIB()
	{
		try { servers = ThingsServers.getInstance(); }
		catch (IOException e) { throw new RuntimeException(e); }
	}

	public static final String MAXEVENTS_ENTRY = "org.crypthing.things.maxevents";
	public static final String LOG_ENTRY = "org.crypthing.things.eventLog";
	public static final String MIBADDRESS = "java:global/things/MIBAdddress";

	/**
	 * MIB initialization. Possible configurations:<br>
	 * <ul>
	 * 	<li>maxevents: maximum amount of events to keep. Default value: 128;</li>
	 * 	<li>eventLog: events of type Info, Warning and Error should be logged. Default value: false.</li>
	 * </ul>
	 * <p>Any property started with CommandResponder. will be used to configure SNMP listener.</p>
	 * @param config: configuration properties
	 * @throws MIBInitException if something goes wrong
	 */
	public void init(final Properties config) throws MIBInitException
	{
		Notary.init(Integer.parseInt(config.getProperty(MAXEVENTS_ENTRY, "128")));
		mustLog = Boolean.parseBoolean(config.getProperty(LOG_ENTRY));
		LOG.log(Level.INFO, "Notary is awake!");
		
		try { mibAddress = (String) new InitialContext().lookup(MIBADDRESS); }
		catch (final Exception e) { throw new MIBInitException("Could not locate the resource:" + MIBADDRESS, e); }
		trap = new TrapReceiver();
		try
		{
			trap.receive(mibAddress, this);
			Enumeration<?> parameters = config.propertyNames();
			while(parameters.hasMoreElements())
			{
				final String el = (String) parameters.nextElement();
				if (el.startsWith("CommandResponder."))
				{
					trap.addCommandResponder(Class.forName(config.getProperty(el)).asSubclass(CommandResponder.class).newInstance());
					// Se explodir aqui, é melhor que pare mesmo, o pacote está mal-formado.
				}
			}
		}
		catch (final Throwable e)
		{
			if (trap != null) { try { trap.stop(); } catch (final Exception e1){ LOG.log(Level.SEVERE, "Unexpected error:", e1);}}
			throw new MIBInitException("Could not start MIB due to following error", e);
		}	
		running = true;
		new Thread(new WatchDog()).start();
	}
	/**
	 * Releases listener.
	 */
	public void destroy()
	{
		running = false;
		try
		{
			if (trap != null) trap.stop();
			ThingsServers.getInstance().store();
		}
		catch (final IOException e) { LOG.log(Level.SEVERE, "An error occurred while saving servers file.", e); }
	};

	@Override
	public void processPdu(final CommandResponderEvent evt)
	{
		final PDU command = evt.getPDU();
		if (command == null) return;
		try
		{
			final PDUMap map = PDUMap.parse(command);
			if (map == null) return;
			boolean rogue = false;
			Server server = servers.getServer(map.getServername());
			if (server == null) { server = servers.getAgent(map.getServername()); }
			if (server == null)
			{
				server = servers.getRogueServer(map.getServername());
				if(server == null)
				{
					rogue = true;
					server = new Server();
					server.setName(map.getServername());
				}
			}
			server.setLastknowof(map.getTimestamp());
			Notary.register(server, map);
			switch (map.getType())
			{
			case Heart:
				server.beat();
				if (map.getHeartbeat() != null) 
				{
					final HeartBeat heart = map.getHeartbeat();
					if (rogue)
					{
						if (heart.getType() == HeartBeat.Type.AGENT) { servers.addAgent(server); }
						else { servers.addRogueServer(server); }
					}
					server.setJmx(heart.getJmx());
				}
				break;
			case Stop:
				final String contents;
				if ((contents = map.getContents()) != null && (contents.contains(AGENT_SHORTCUT) || contents.contains(SERVICE_SHORTCUT)))
				{
					try	// A stop signal has been received from the agent or service. We must prepare to stop monitoring.
					{
						final JSONObject signal = new JSONObject(contents);
						if (signal.getString("who").contentEquals(AGENT) || signal.getString("who").contentEquals(SERVICE)) server.shutdownSignal();
					}
					catch (final Exception swalloed ) { /* We will only deal with Things version 2.6.0 or above. All others will be left behind. */ }
				}
				break;
			case Info:
			case Warning:
			case Error:
				if (mustLog) log(map);
				break;
			default:
				break;
			}
		}
		catch (IOException e) { LOG.log(Level.WARNING, "Problems in paradise:",  e); }
	}
	private void log(final PDUMap pdu)
	{
		try
		{
			final JSONObject signal = new JSONObject(pdu.getContents());
			Level level;
			switch (pdu.getType())
			{
			case Info:		level = Level.INFO; break;
			case Warning:	level = Level.WARNING; break;
			case Error:		level = Level.SEVERE; break;
			default:		level = Level.INFO;
			}
			final Logger alien = Logger.getLogger(signal.getString("who"));
			final StringBuilder builder = new StringBuilder(2048);
			builder.append(signal.getString("message"));
			if (level == Level.SEVERE) builder.append("\n").append("Caused by: ").append(signal.getString("stacktrace"));
			alien.log(level, builder.toString());
		}
		catch (final Exception swalloed) { LOG.warning("A malformed event signal was received to log: " + pdu.getContents()); }
	}
	
	private class WatchDog implements Runnable
	{
		@Override
		public void run()
		{
			try { Thread.sleep(DELAY); } catch (final InterruptedException e1) {}
			LOG.info("And before them a dreaded hound, on watch, who has no pity, but a vile stratagem: as people go in he fawns on all, with actions of his tail and both ears, but he will not let them go back out, but lies in wait for them and eats them up, when he catches any going back through the gates.");
			while (running)
			{
				try
				{
					Server[] temp = new Server[0];
					temp = servers.getServers().values().toArray(temp);
					for (int i = 0; i < temp.length; i++)
					{
						if (!temp[i].isShutingdown()) JMXConnectorHelper.updateServerStatus(temp[i]);
						else servers.removeServer(temp[i]);
					}
					// For now, we only remove the dead agent from its list.
					temp = new Server[0];
					temp = servers.getAgents().values().toArray(temp);
					for (int i = 0; i < temp.length; i++) if (temp[i].isShutingdown()) servers.removeAgent(temp[i]);
					Thread.sleep(DELAY);
				}
				catch(Exception e) { LOG.log(Level.SEVERE, "And there, a dread sight even for gods to see, was Kerberos:", e); }
			}
			LOG.info("O, you most shameless desperate ruffian, you O, villain, villain, arrant vilest villain! Who seized our Kerberos by the throat, and fled, and ran, and rushed, and bolted, haling of the dog, my charge!");
		}
	}

	/**
	 * Exception thrown on MIB initialization failure
	 */
	public static class MIBInitException extends Exception
	{
		private static final long serialVersionUID = 9154307549902806365L;
		public MIBInitException(final String msg, final Throwable e) { super(msg, e); }
	}
}
