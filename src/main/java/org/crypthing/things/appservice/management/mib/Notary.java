package org.crypthing.things.appservice.management.mib;

import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;

import org.crypthing.things.appservice.management.data.Server;

/**
 * Events record facility.
 */
public class Notary
{

    private static Map<Server,Deque<Object>> events = new HashMap<Server,Deque<Object>>();
    private static boolean init = false;
    private static int max_len;
    private static final int GAP = 16;

	/**
	 * Initializes events registry
	 * @param capacity: record capacity
	 */
    public static void init(final int capacity) { max_len = capacity; init = true; }
	/**
	 * Records a new event from application
	 * @param server: application bean
	 * @param event: event
	 */
    public static void register(final Server server, final Object event)
    {
        verify();
        final Deque<Object> deque = getDeque(server);
        for(int i = 0; i < (deque.size() - max_len);) deque.pollLast();
        //Try to avoid entering on synchronized code, server must be very crazy to blow it up.
        boolean done = deque.offerFirst(event);
        if(!done)
        {
            synchronized(server)
            {
                deque.pollLast();
                deque.addFirst(event); // This one could blow up... I expect synchronization be up to the task
            }
        }
	}
	/**
	 * Gets all events received from an application
	 * @param server: application bean
	 * @return recorded vents
	 */
    public static Object[] getEvents(final Server server)
    {
        verify();
        final Deque<Object> deque = getDeque(server);
        return deque.toArray();
    }

	private static void verify() { if(!init) throw new IllegalStateException("Notary must be initialized"); }
    private static Deque<Object> getDeque(Server server)
    {
        Deque<Object> deque = events.get(server);
        if(deque == null)
        {
            deque = new LinkedBlockingDeque<Object>(max_len + GAP);
            events.put(server, deque);
        }
        return deque;
    }
}