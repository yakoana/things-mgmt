package org.crypthing.things.appservice.management.mib;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.crypthing.things.appservice.management.data.HeartBeat;
import org.snmp4j.PDU;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Converts an SNMP trap into a application bean
 */
public class PDUMap
{
	/**
	 * Events
	 */
	public enum Type
	{
		/**
		 * Application start
		 */
		Start,
		/**
		 * Application task
		 */
		Work,
		/**
		 * Application end
		 */
		Stop,
		/**
		 * Heartbeat signal
		 */
		Heart,
		/**
		 * Informative message
		 */
		Info,
		/**
		 * Warning message
		 */
		Warning,
		/**
		 * Error message
		 */
		Error
	}
	/**
	 * Parses SNMP PDU into a bean
	 * @param command: trap
	 * @return bean
	 * @throws IOException if somethings goes wrong
	 */
	public static PDUMap parse(final PDU command) throws IOException
	{
		if (command == null) return null;
		final PDUMap map = new PDUMap();
		final OID root = new OID(command.getVariable(SnmpConstants.sysObjectID).toString());
		map.setRoot(root.toDottedString());
		final OID data = ((OID) root.clone()).append(1);
		List<VariableBinding> vList = command.getBindingList(data);
		for (VariableBinding unit : vList)
		{
			final OID doid = unit.getOid();
			switch (doid.get(data.size()))
			{

				case 1:
					map.timestamp = unit.getVariable().toLong();
					break;
				case 2:
					map.servername = unit.getVariable().toString();
					break;
				case 3:
					map.jbossname = unit.getVariable().toString();
					break;
				default:
					break;
			}
		}
		OID evt = ((OID) root.clone()).append(2);
		vList = command.getBindingList(evt);
		if (vList.size() > 0)
		{
			final VariableBinding vb = vList.get(0);
			final OID oid = vb.getOid();
			switch(oid.get(evt.size()))
			{
			case 1: map.type = Type.Start; break;
			case 2: map.type = Type.Work; break;
			case 3: map.type = Type.Stop; break;
			case 4:
				map.type = Type.Heart;
				ObjectMapper mapper = new ObjectMapper();
				map.heartbeat = mapper.readValue(vb.getVariable().toString(), HeartBeat.class);
			}
			map.contents = new String(((OctetString) vb.getVariable()).getValue(), StandardCharsets.UTF_8);
		}
		evt = ((OID) root.clone()).append(3);
		vList = command.getBindingList(evt);
		if (vList.size() > 0)
		{
			final VariableBinding vb = vList.get(0);
			final OID oid = vb.getOid();
			switch(oid.get(evt.size()))
			{
			case 1: map.type = Type.Info; break;
			case 2: map.type = Type.Warning; break;
			case 3: map.type = Type.Error;
			}
			map.contents = new String(((OctetString) vb.getVariable()).getValue(), StandardCharsets.UTF_8);
		}
		return map;
	}

	private Type type;
	private String root;
	private long timestamp;
	private String servername;
	private String jbossname;
	private HeartBeat heartbeat;
	private String contents;

	/**
	 * Gets message contents, if any
	 * @return contents or null
	 */
	public String getContents() { return contents; }
	/**
	 * Set message contents
	 * @param contents: message
	 */
	public void setContents(final String contents) { this.contents = contents; }
	/**
	 * Gets message type
	 * @return type
	 */
	public Type getType() { return type; }
	/**
	 * Sets message type
	 * @param type: the type
	 */
	public void setType(final Type type) { this.type = type;}
	/**
	 * Gets trap root OID
	 * @return OID
	 */
	public String getRoot() { return root; }
	/**
	 * Sets trap root OID
	 * @param root: OID
	 */
	public void setRoot(final String root) { this.root = root; }
	/**
	 * Gets message timestamp
	 * @return timestamp
	 */
	public long getTimestamp() { return timestamp; }
	/**
	 * Sets message timestamp
	 * @param timestamp: message timestamp
	 */
	public void setTimestamp(final long timestamp) { this.timestamp = timestamp; }
	/**
	 * Gets the name of application that sent the message
	 * @return application name
	 */
	public String getServername() { return servername; }
	/**
	 * Sets the application name
	 * @param servername: application name
	 */
	public void setServername(final String servername) { this.servername = servername; }
	/**
	 * Gets originator application server name, if any
	 * @return AS name
	 */
	public String getJbossname() { return jbossname; }
	/**
	 * Sets originator application server name
	 * @param jbossname: AS name
	 */
	public void setJbossname(String jbossname) { this.jbossname = jbossname; }
	/**
	 * Gets heartbeat message
	 * @return the heartbeat ben
	 */
	public HeartBeat getHeartbeat() { return heartbeat; }
	/**
	 * Sets the message heartbeat ben
	 * @param heartbeat: the bean
	 */
	public void setHeartbeat(final HeartBeat heartbeat) { this.heartbeat = heartbeat; }	
}
