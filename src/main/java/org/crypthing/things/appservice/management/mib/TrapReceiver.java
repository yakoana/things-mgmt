package org.crypthing.things.appservice.management.mib;

import java.io.IOException;

import org.snmp4j.CommandResponder;
import org.snmp4j.MessageDispatcher;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.MPv2c;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.MultiThreadedMessageDispatcher;
import org.snmp4j.util.ThreadPool;

/**
 * Implements an SNMP trap receiver. Facade to SNMP4J library.
 */
public class TrapReceiver 
{
	
	private Snmp snmp;
	private TransportMapping<?> transport;

	/**
	 * Start traps listening
	 * @param mibAddr: MIB address. Default value: 127.0.0.1/8163 (for debugging)
	 * @param responder: PDU processor.
	 * @throws Throwable any error
	 */
	public void receive(final String mibAddr, CommandResponder responder) throws Throwable
	{
		final String ip = mibAddr != null ? mibAddr : "127.0.0.1/8163";
		ThreadPool threadPool = ThreadPool.create("DispatcherPool", 10);
		MessageDispatcher mtDispatcher = new MultiThreadedMessageDispatcher(threadPool, new MessageDispatcherImpl());
		mtDispatcher.addMessageProcessingModel(new MPv2c());
		Address targetAddress = GenericAddress.parse("udp:" + ip);
		transport = new DefaultUdpTransportMapping((UdpAddress)targetAddress,true);
		snmp = new Snmp(mtDispatcher, transport);
		snmp.addCommandResponder(responder);
		transport.listen();
		snmp.listen();
	}
	/**
	 * Adds a new PDU processor
	 * @param responder: PDU processor
	 */
	public void addCommandResponder(final CommandResponder responder) { if(isListening()) snmp.addCommandResponder(responder); }
	/**
	 * Checks if listener is active
	 * @return true if it is listening
	 */
	public boolean isListening() { return (snmp != null && transport != null  && transport.isListening()); }
	/**
	 * Stops listening
	 * @throws IOException on error
	 */
	public void stop() throws IOException {	snmp.close(); }

}    
