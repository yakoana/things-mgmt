package org.crypthing.things.appservice.management.rest;

import java.io.IOException;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.crypthing.things.appservice.management.data.Server;
import org.crypthing.things.appservice.management.data.ThingsServers;
import org.crypthing.things.appservice.management.mib.Notary;

/**
 * Things agents report service: [server]/api/agents
 */
@Path("agents")
public class AgentRest
{
	private ThingsServers things;

	/**
	 * Creates a new instance of service
	 */
	public AgentRest()
	{
		try { things = ThingsServers.getInstance(); }
		catch (final IOException e) { throw new RuntimeException(e); }
	}
	/**
	 * Gets a set of known agents
	 * @return set of agents names
	 */
	@GET
	@Produces("application/json")
	public Set<String> getServers() { return things.getAgents().keySet(); }
	/**
	 * Gets information about an agent
	 * @param name: agent name. Must be one of those returned by {@link #getServers}
	 * @return agent info. See {@link org.crypthing.things.appservice.management.data.Server}
	 */
	@GET
	@Path("{name}")
	@Produces("application/json")
	public Response getServer(@PathParam("name") String name)
	{
		final Server server = things.getAgent(name);
		if(server == null )
		{
			return Response.status(Response.Status.NOT_FOUND).entity("{ \"message\" : \"No such server.\" }" ).build();
		}
		return Response.status(Response.Status.OK).entity(server).build();
	}
	/**
	 * Gets events received from an agent
	 * @param name: agent name. Must be one of those returned by {@link #getServers}
	 * @return an array of JSON events
	 */
	@GET
	@Path("{name}/events")
	@Produces("application/json")
	public Object[] getServers(@PathParam("name") String name)
	{
		Server server = things.getAgent(name);
		return Notary.getEvents(server);
	}
}
