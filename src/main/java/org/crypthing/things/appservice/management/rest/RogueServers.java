package org.crypthing.things.appservice.management.rest;

import java.io.IOException;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.crypthing.things.appservice.management.data.Server;
import org.crypthing.things.appservice.management.data.ThingsServers;

/**
 * Non-monitoring (rogue) applications service: [server]/api/rogueservers
 */
@Path("rogueservers")
public class RogueServers
{
	private ThingsServers things;

	/**
	 * Creates a new instance
	 */
	public RogueServers()
	{
		try { things = ThingsServers.getInstance(); }
		catch (final IOException e) { throw new RuntimeException(e); }
	}
	/**
	 * Gets a set of known rogues
	 * @return a set of rogue names
	 */
	@GET
	@Produces("application/json")
	public Set<String> getRogueServers() { return things.getRogueServers().keySet(); }
	/**
	 * Gets information about a rogue application
	 * @param name: application name. Must be one of those returned by {@link #getRogueServers}
	 * @return application info. See {@link org.crypthing.things.appservice.management.data.Server}.
	 */
	@GET
	@Path("{name}")
	@Produces("application/json")
	public Response getServer(@PathParam("name") String name)
	{
		final Server server = things.getRogueServer(name);
		if(server == null )
		{
			return Response.status(Response.Status.NOT_FOUND).entity("{ \"message\" : \"No such server.\" }" ).build();
		}
		return Response.status(Response.Status.OK).entity(server).build();
	}
	/**
	 * Promotes a rogue application to monitoring status
	 * @param name: application name. Must be one of those returned by {@link #getRogueServers}
	 * @return action report.
	 */
	@GET
	@Path("{name}/promote")
	@Produces("application/json")
	public Response promote(@PathParam("name") String name)
	{
		Server rogue = things.getRogueServer(name);
		if (rogue == null )
		{
			return Response.status(Response.Status.NOT_FOUND).entity("{ \"message\" : \"No such server in rogues, perhaps it escaped.\" }").build();
		}
		if (things.getServer(name) != null )
		{
			return Response.status(Response.Status.NOT_ACCEPTABLE).entity("{ \"message\" : \"A server with this name is already in Servers database.\" }").build();
		}
		things.addServer(rogue);
		things.removeRogueServer(rogue);
		return Response.status(Response.Status.OK).entity("{ \"message\" : \"Another rogue is promoted.\" }").build();
	}
}
