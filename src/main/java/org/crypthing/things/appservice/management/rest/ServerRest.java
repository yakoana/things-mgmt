package org.crypthing.things.appservice.management.rest;

import java.io.IOException;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.crypthing.things.appservice.management.data.Server;
import org.crypthing.things.appservice.management.data.ThingsServers;
import org.crypthing.things.appservice.management.mib.Notary;

/**
 * Monitoring applications service: [server]/api/servers
 */
@Path("servers")
public class ServerRest
{
	private ThingsServers things;

	/**
	 * Creates a new instance of the service
	 */
	public ServerRest()
	{
		try { things = ThingsServers.getInstance(); }
		catch (IOException e) { throw new RuntimeException(e); }
	}
	/**
	 * Gets a set of known applications
	 * @return a set of application names
	 */
	@GET
	@Produces("application/json")
	public Set<String> getServers() { return things.getServers().keySet(); }
	/**
	 * Gets information about an application
	 * @param name: application name. Must be one of those returned by {@link #getServers}
	 * @return application info. See {@link org.crypthing.things.appservice.management.data.Server}
	 */
	@GET
	@Path("{name}")
	@Produces("application/json")
	public  Response getServer(@PathParam("name") String name)
	{
		Server server = things.getServer(name);
		if(server == null)
		{
			return Response.status(Response.Status.NOT_FOUND).entity("{ \"message\" : \"No such server.\" }" ).build();
		}
		return Response.status(Response.Status.OK).entity(server).build();
	}
	/**
	 * Gets events received from an application
	 * @param name: application name. Must be one of those returned by {@link #getServers}
	 * @return an array of JSON events
	 */
	@GET
	@Path("{name}/events")
	@Produces("application/json")
	public Object[] getServers(@PathParam("name") String name)
	{
		Server server = things.getServer(name);
		return Notary.getEvents(server);
	}
	/**
	 * Demotes an application from its monitoring status
	 * @param name: application name. Must be one of those returned by {@link #getServers}
	 * @return action report.
	 */
	@GET
	@Path("{name}/demote")
	@Produces("application/json")
	public Response demote(@PathParam("name") String name)
	{
		final Server convict = things.getServer(name);
		if (convict == null )
		{
			return Response.status(Response.Status.NOT_FOUND).entity("{ \"message\" : \"No such server in servers, watch out who you accuse.\" }" ).build();
		}
		if (things.getRogueServer(name) !=  null )
		{
			return Response.status(Response.Status.NOT_ACCEPTABLE).entity("{ \"message\" : \"A server with this name is already in rogue database.\" }").build();
		}
		things.removeServer(convict);
		things.addRogueServer(convict);
		return Response.status(Response.Status.OK).entity("{ \"message\" : \"Another rogue goes to jail.\" }").build();
	}
}
