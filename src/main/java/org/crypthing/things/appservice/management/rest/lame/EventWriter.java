
package org.crypthing.things.appservice.management.rest.lame;

import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import org.json.JSONObject;
import org.json.JSONWriter;


@Provider
@Produces("application/json")
public class EventWriter implements MessageBodyWriter<Object>
{

	@Override
	public boolean isWriteable(final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType)
	{
		return Object.class.isAssignableFrom(type);
	}

	@Override
	public long getSize(final Object t, final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType)
	{
		return -1;
	}

	@Override
	public void writeTo
	(
		final Object t,
		final Class<?> type,
		final Type genericType,
		final Annotation[] annotations,
		final MediaType mediaType,
		final MultivaluedMap<String, Object> httpHeaders,
		final OutputStream entityStream
	)	throws IOException, WebApplicationException
	{
		try
		{
			final Object[] array = (Object[]) t;
			final StringWriter writer = new StringWriter(4096);
			final JSONWriter json = new JSONWriter(writer);
			json.array();
			for (int i = 0; i < array.length; i++) json.value(new JSONObject(array[i]));
			json.endArray();
			entityStream.write(writer.toString().getBytes(StandardCharsets.UTF_8));
		}
		catch (final RuntimeException e) { throw new WebApplicationException(e); }
	}
	
}