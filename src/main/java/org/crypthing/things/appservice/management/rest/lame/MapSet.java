package org.crypthing.things.appservice.management.rest.lame;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;


@Provider
@Produces("application/json")
public class MapSet implements MessageBodyWriter<Set<String>> {

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return Set.class.isAssignableFrom(type);
    }

    @Override
    public long getSize(Set<String> t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return -1;
    }
    

    @Override
    public void writeTo(Set<String> t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType,
            MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
            throws IOException, WebApplicationException {
                StringBuilder sb = new StringBuilder();
                sb.append("[");
                boolean first = true;
                for(String s : t)
                {
                    if(!first) sb.append(","); else first = false;
                    sb.append("\"" + s.replaceAll("\"", "\\\"") +"\"");
                }
                sb.append("]");
                entityStream.write(sb.toString().getBytes(StandardCharsets.UTF_8)); 
    }
    
}