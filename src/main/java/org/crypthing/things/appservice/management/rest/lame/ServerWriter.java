package org.crypthing.things.appservice.management.rest.lame;

import org.crypthing.things.appservice.management.data.Server;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

@Provider
@Produces("application/json")
public class ServerWriter implements MessageBodyWriter<Server>
{

	@Override
	public boolean isWriteable(final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType)
	{
		return Server.class.isAssignableFrom(type);
	}

	@Override
	public long getSize(final Server t, final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType)
	{
		return -1;
	}

	@Override
	public void writeTo
	(
		final Server t,
		final Class<?> type,
		final Type genericType,
		final Annotation[] annotations,
		final MediaType mediaType,
		final MultivaluedMap<String, Object> httpHeaders,
		final OutputStream entityStream
	)	throws IOException, WebApplicationException
	{
		StringWriter writer = new StringWriter(2048);
		try
		{
			final JSONObject json = new JSONObject(t);
			json.write(writer);
		}
		catch (final RuntimeException e) { throw new WebApplicationException(e); }
		entityStream.write(writer.toString().getBytes(StandardCharsets.UTF_8));
	}
}
