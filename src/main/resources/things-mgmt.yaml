swagger: "2.0"
info:
  description: "Gerência e Monitoração de Servidores Things"
  version: "1.0.0"
  title: "Thing App Server Monitor"
  termsOfService: ""
  contact:
    email: "diego.sohsten@gmail.com"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"
host: "localhost:8080"
basePath: "/things-mgmt/api/"
tags:
- name: "agents"
  description: "API de acesso aos agentes"
- name: "servers"
  description: "API de servidores reconhecidos"
- name: "rogueservers"
  description: "API sobre servidores descobertos mas não reconhecidos."
schemes:
- "http"
paths:
  /agents:
    get:
      tags:
      - "agents"
      summary: "Lista todos os agentes"
      description: "Lista nome de todos os agentes ativos."
      operationId: ""
      produces:
      - "application/json"
      responses:
        200:
          description: "Operação bem sucedida"
          schema:
            type: "array"
            items:
              type: "string"
      security:
      - things_auth: []
  /agents/{name}:
    get:
      tags:
      - "agents"
      summary: "Exibe um agente."
      description: "Lista os dados de um determinado agente."
      operationId: ""
      produces:
      - "application/json"
      parameters:
        - name: "name"
          in: "path"
          description: "O nome do agente. Ver: /agents"
          required: true
          type: "string"
      responses:
        200:
          description: "Agente Things encontrado"
          schema:
            $ref: "#/definitions/Server"
        404:
          description: "Servidor Things não encontrado"
      security:
      - things_auth: []
  /agents/{name}/events:
    get:
      tags:
      - "agents"
      summary: "Lista os traps SNMP enviados pelo agente."
      description: "Lista os últimos traps SNMP, sendo limitados pela configuração do servidor."
      operationId: ""
      produces:
      - "application/json"
      parameters:
        - name: "name"
          in: "path"
          description: "O nome do agente. Ver: /agents"
          required: true
          type: "string"
      responses:
        200:
          description: "Agente Things encontrado"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Events"
        404:
          description: "Agente Things não encontrado"
      security:
      - things_auth: []
  /servers:
    get:
      tags:
      - "servers"
      summary: "Lista todos os servidores"
      description: "Lista nome de todos os servidores reconhecidos."
      operationId: ""
      produces:
      - "application/json"
      responses:
        200:
          description: "Operação bem sucedida"
          schema:
            type: "array"
            items:
              type: "string"
      security:
      - things_auth: []
  /servers/{servername}:
    get:
      tags:
      - "servers"
      summary: "Exibe um servidor."
      description: "Lista os dados de um determinado servidor."
      operationId: ""
      produces:
      - "application/json"
      parameters:
        - name: "servername"
          in: "path"
          description: "O nome do servidor. Ver: /servers"
          required: true
          type: "string"
      responses:
        200:
          description: "Servidor Things encontrado"
          schema:
            $ref: "#/definitions/Server"
        404:
          description: "Servidor Things não encontrado"
      security:
      - things_auth: []
  /servers/{servername}/demote:
    get:
      tags:
      - "servers"
      summary: "Rebaixa um servidor conhecido para não conhecido."
      description: "Retira o servidor da lista de conhecidos e coloca na lista de desconhecidos."
      operationId: ""
      produces:
      - "application/json"
      parameters:
        - name: "servername"
          in: "path"
          description: "O nome do servidor. Ver: /servers"
          required: true
          type: "string"
      responses:
        200:
          description: "Servidor Things encontrado"
        404:
          description: "Servidor Things não encontrado"
      security:
      - things_auth: []
  /servers/{servername}/events:
    get:
      tags:
      - "servers"
      summary: "Lista os traps SNMP enviados pelo servidor."
      description: "Lista os últimos traps SNMP, sendo limitados pela configuração do servidor."
      operationId: ""
      produces:
      - "application/json"
      parameters:
        - name: "servername"
          in: "path"
          description: "O nome do servidor. Ver: /servers"
          required: true
          type: "string"
      responses:
        200:
          description: "Servidor Things encontrado"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Events"
        404:
          description: "Servidor Things não encontrado"
      security:
      - things_auth: []
  /rogueservers:
    get:
      tags:
      - "rogueservers"
      summary: "Lista todos os servidores não reconhecidos"
      description: "Lista nome de todos os servidores não reconhecidos."
      operationId: ""
      produces:
      - "application/json"
      responses:
        200:
          description: "Operação bem sucedida"
          schema:
            type: "array"
            items:
              type: "string"
      security:
      - things_auth: []
  /rogueservers/{servername}:
    get:
      tags:
      - "rogueservers"
      summary: "Exibe um servidor."
      description: "Lista os dados de um determinado servidor."
      operationId: ""
      produces:
      - "application/json"
      parameters:
        - name: "servername"
          in: "path"
          description: "O nome do servidor. Ver: /rogueservers"
          required: true
          type: "string"
      responses:
        200:
          description: "Servidor encontrado"
          schema:
            $ref: "#/definitions/Server"
        404:
          description: "Servidor não encontrado"
      security:
      - things_auth: []
  /rogueservers/{servername}/promote:
    get:
      tags:
      - "rogueservers"
      summary: "Promove um servidor de não conhecido para conhecido."
      description: "Retira o servidor da lista de rogues e coloca na lista de conhecidos. Os servidores conhecidos são persistidos."
      operationId: ""
      produces:
      - "application/json"
      parameters:
        - name: "servername"
          in: "path"
          description: "O nome do servidor. Ver: /rogueservers"
          required: true
          type: "string"
      responses:
        200:
          description: "Servidor encontrado"
        404:
          description: "Servidor não encontrado"
      security:
      - things_auth: []
securityDefinitions:
  things_auth:
    type: basic
definitions:
  JMX:
    type: "object"
    properties:
      address:
        type: "string"
      port:
        type: "integer"
        format: "int32"
  Server:
    type: "object"
    properties:
      name:
        type: "string"
      config:
        type: "string"
      env:
        type: object
        additionalProperties: true
      jmx:
        $ref: "#/definitions/JMX"
      lastknowof:
        type: "integer"
        format: "int64"
      startTime:
        type: "integer"
        format: "int64"
      success:
        type: "integer"
        format: "int64"
      failures:
        type: "integer"
        format: "int64"
      workers:
        type: "integer"
        format: "int32"
      heap:
        type: "object"
        properties:
          init:
            type: "integer"
            format: "int64"
          used:
            type: "integer"
            format: "int64"
          committed:
            type: "integer"
            format: "int64"
          max:
            type: "integer"
            format: "int64"
      nonheap:
        type: "object"
        properties:
          init:
            type: "integer"
            format: "int64"
          used:
            type: "integer"
            format: "int64"
          committed:
            type: "integer"
            format: "int64"
          max:
            type: "integer"
            format: "int64"
      heartbeats:
            type: "integer"
            format: "int64"
  Events:
    type: "object"
    properties:
      type:
        type: "string"
      root:
        type: "string"
      timestamp:
        type: "integer"
        format: "int64"
      servername:
        type: "string"
      jbossname:
        type: "string"
      heartbeat:
        type: "object"
        properties:
          jmx:
            $ref: "#/definitions/JMX"
          success:
            type: "integer"
            format: "int64"
          failures:
            type: "integer"
            format: "int64"
          workers:
            type: "integer"
            format: "int32"
      contents:
        type: "string"